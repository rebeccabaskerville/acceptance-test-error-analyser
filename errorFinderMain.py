import sys
error = sys.argv[1]

# STAGE 1
#Splits the full string at 3 main points to get the comparison expected and result sections
startEx = error.index("<[",0)
endEx = error.index("]>",0)
expected = error[startEx:endEx]

start = error.index("but was:",0)
end = error.index("end",0)
result = error[start:end]



# STAGE 2
#Extracts the expected and result rows from the input error string
expected = expected.split("}, {")
expectedRows = []
for row in expected:
    row = row.strip("<[{")
    row = row.removesuffix("}")
    expectedRows.append(row)


result = result.split("}, {")
actualRows = []
for each in result:
    each = each.removeprefix("but was: <[{")
    each = each.removesuffix("}]> ")
    actualRows.append(each)



# STAGE 3
# Create dictionaries of the expected and actual results
    expected = dict()
    actual = dict()

def extractValues(rows, dictionary):    
    for each in rows:
        values = each.split(", ")
        for x in values:
            x = x.split("=")
            # x[0]=column and x[1] = value
            if x[0] not in dictionary.keys():
                dictionary[x[0]]= [] 
                dictionary[x[0]].append(x[1])
            else:
                dictionary[x[0]].append(x[1])    

extractValues(expectedRows,expected)
extractValues(actualRows,actual)


# STAGE 4 - Compares expected and actual results and prints suggested fixes
expectedLen = len(expectedRows)
actualLen = len(actualRows)

print("\nERROR ANALYSIS\n")

for key in expected:
    if key in actual:
        # Want to compare 2 arrays
        if expectedLen != actualLen:
                print("\nERROR FOUND: There is a difference in the number of expected and actual rows found")
                print("Actual rowcount is: " + str(actualLen) + " but the expected rowcount was " + str(expectedLen))  
        else:
            if set(expected[key]) != set(actual[key]): 
                set_diff = set(actual[key]).difference(set(expected[key]))
                set_diff2 = set(expected[key]).difference(set(actual[key]))
                difference = list(set_diff.union(set_diff2))
                
                print("Check column '" + str(key) + "'")
                print("Actual results were " + str(actual[key]))
                print("Expected results were " + str(expected[key]))
                print("Differences found were " + str(difference) + "\n")          
    else:
        print("\nERROR FOUND: A different column name has been found in the actual results compared to expected. Check the following column exists:")
        print(key)

print("\n---------------------------------------------------------------------------------------------------------------------------------")   

# Prints the actual result rows in cucumber format
print("The actual results were: ")
# Extracts the columns
columns = []
columnString = " | "
for key in actual:
    columns.append(key)
for each in columns:
    columnString = columnString + str(each) + " | "
print(columnString)

# Extracts the row values and prints in cucumber format
row = dict()
for key in actual:
    i = 0
    while i < len(actual[key]):
        if i not in row.keys():
            row[i] = [] 
            row[i].append(actual[key][i])
        else:
            row[i].append(actual[key][i]) 
        i = i+1         
for key in row:
    rowString = " | "
    for each in row[key]:
        rowString = rowString + str(each) + " | "
    print(rowString)    
  
    