**Guide to using the error analysis tool**

Clone the repository to download the tool. The directory should contain the following files;
- errorFinder.sh (A shell script to run the tool)
- errorFinderMain.py (Python script performing the analysis)

---

## Purpose of the tool

The tool can be used to quickly analyse sampling assertion errors thrown in the cucumber acceptance tests. 
An example of this type of error is included below as it appears in intelliJ: 

Step failed
org.opentest4j.AssertionFailedError: expected: <[{group=group_b, count_int=7, count_num=5, count_float=5, count_text=4, count_ts=5, count_date=5, count_bool=5}, {group=group_a, count_int=7, count_num=7, count_float=7, count_text=7, count_ts=7, count_date=7, count_bool=7}]> but was: <[{count_text=4, count_num=5, count_int=5, count_date=5, count_ts=5, count_float=5, count_bool=5, group=group_b}, {count_text=7, count_num=7, count_int=7, count_date=7, count_ts=7, count_float=7, count_bool=7, group=group_a}]>
	at org.junit.jupiter.api.AssertionUtils.fail(AssertionUtils.java:55)
	at org.junit.jupiter.api.AssertionUtils.failNotEqual(AssertionUtils.java:62)
	at org.junit.jupiter.api.AssertEquals.assertEquals(AssertEquals.java:182)
	at org.junit.jupiter.api.AssertEquals.assertEquals(AssertEquals.java:177)
	at org.junit.jupiter.api.Assertions.assertEquals(Assertions.java:1141)
	at com.matillion.bi.emerald.shared.model.util.SampleResultAssertions.assertRowsAreInAnyOrder(SampleResultAssertions.java:60)
	at com.matillion.bi.emerald.component.acceptance.driver.DwhDriver.assertQueryResultsAreInAnyOrder(DwhDriver.java:50)
	at com.matillion.bi.emerald.component.acceptance.StepDefinitions.sampling_results_are_in_any_order(StepDefinitions.java:245)
	at ✽.sampling results are in any order:(file:///home/rebecca/emerald/emerald-cucumber/src/main/resources/com/matillion/bi/emerald/component/deltalake/deltalake-shared/link/aggregate.feature:44)
 

---

## Using the tool

Using the example error shown above; the steps for using the tool are:

1. Using the terminal, navigate to the directory containing both files as stated above
2. Run the command: bash errorFinder.sh
3. The shell script will prompt you to enter the error. Only a portion of the error message is required and this can be copied and pasted into the terminal,
this should be in the format as shown below and should only include this section:

expected: <[{group=group_b, count_int=7, count_num=5, count_float=5, count_text=4, count_ts=5, count_date=5, count_bool=5}, {group=group_a, count_int=7, count_num=7, count_float=7, count_text=7, count_ts=7, count_date=7, count_bool=7}]> but was: <[{count_text=4, count_num=5, count_int=5, count_date=5, count_ts=5, count_float=5, count_bool=5, group=group_b}, {count_text=7, count_num=7, count_int=7, count_date=7, count_ts=7, count_float=7, count_bool=7, group=group_a}]>

4. The script will run and results will be returned

---

## The results

If the errors have been successfully identified then details will be returned for each error under the ERROR ANALYSIS section
The analysis will identify the column of data containing the issue and display the expected and actual results
Additionally all of the actual results and columns will be returned in cucumber syntax so that they can easily be copied and pasted to update or fix the failing cucumber scenario
