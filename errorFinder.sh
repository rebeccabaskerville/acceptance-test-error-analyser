#!/bin/sh
echo "Enter acceptance test error (Note: The error string must be in the format expected: <[{ <results> but was: <results> }]>)"
read error
SUFFIX=" end" 
ERROR="$error$SUFFIX"
python3.9 errorFinderMain.py "$ERROR"
